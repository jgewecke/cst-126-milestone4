<?php
// Project Name: Milestone4
// Project Version: 1.3
// Module Name: Deploying and Hosting Your Website
// Module Version: 1.3
// Programmer Name: Justin Gewecke
// Date: 7/19/2020
// Description: This module handles the admin portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once('myfuncs.php');

$link = dbConnect();

$sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT, TAG FROM posts";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);



if ($numRows >= 1)
{
    foreach($result as $i) {
        $sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT FROM posts"; 
        
        $postID = $i["ID"];
        $text = $i["TEXT"];
        $title = $i["TITLE"];
        $authorID = $i["AUTHOR_ID"];
        $tag = $i["TAG"];

        // Get First and Last name of Author from AUTHOR_ID
        $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$authorID'";
        $i = mysqli_query($link, $sql);
        $row = $i->fetch_assoc();	// Read the Row from the Query
        $author = $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"];
        
        // Display result
        echo nl2br("<h1>$title</h1> <h2>by $author</h2> <p>$text</p> <p id='tag'>Tag: $tag</p>");

        
        if (getUserArrayFromCurrentUser($link)["ADMIN"])
        {
            // Create Admin Panel if player is admin
            echo('<form action="adminPanel.php" method="POST">
				     <input type="hidden" name="Data" value="'.$postID.'"/>
			         <input type="submit" value="Edit in Admin Panel" />
		          </form>');
        }
    }
}

// Close connection
mysqli_close($link);
?>

<a href="index.html">Return to Main Menu.</a>